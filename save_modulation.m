function save_modulation(filename, m, te)
% save_modulation(filename, m, te)
%
% Save modulation as individual images to be loaded on a SLM
%
% Jerome Boulanger 2018

f = fopen(filename,'w');
fprintf(f, 'Normal Mode\n');
for l=1:size(m,3)
    imgname = strrep(filename,'.txt',sprintf('%02d.png',l));
    fprintf(f, '%s,1,%d,0,1,1,1\n', imgname, te);
    imwrite(uint8(m(:,:,l)), imgname);
end
fclose(f);
