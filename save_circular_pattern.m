% Generate a circular pattern

period = [6,9,12,13,14,24];
dims = [1024, 1280];

[x,y] = meshgrid(1:dims(2),1:dims(1));

d = sqrt((x-dims(2)/2).^2 + (y-dims(1)/2).^2);

for k = 1:numel(period)
  M = mod(d/period(k), 1) < 1/2;
  imwrite(logical(M),  sprintf('data/circle-p%02d.png', period(k)));
 end