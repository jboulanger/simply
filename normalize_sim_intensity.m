function [ndata,p] = normalize_sim_intensity(data, otf)
% function data = normalize_sim_intensity(data, otf) 
%
% Normalize intensity using a low-pass filtered image based on the OTF
%
% Jerome Boulanger

avg = mean(data,3);
H = otf;
H = max(otf-0.5,0);
H = H / H(1,1);
ndata = data;
for iter = 1:20
    avg = real(ifft2(H .* fft2(mean(ndata,3))));
    tmp = real(ifft2(H .* fft2(ndata)));
    for k = 1:size(data,3)            
        p(k,:) = polyfit(avg(:), tmp(:,:,k), 1);        
        ndata(:,:,k) = (ndata(:,:,k)-p(k,2))/p(k,1);        
    end    
    if max(sum(p - [1,0],2)) < 1e-5
        fprintf('Stopping after %d iterations.\n', iter)
        break
    end
end

for k = 1:size(data,3)   
    p(k,:) = polyfit(ndata(:,:,k), data(:,:,k), 1);
end
