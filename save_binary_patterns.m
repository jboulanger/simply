% Save binary patterns
%
% Generate various binary patterns and save them to individuals files
%
% Jerome Boulanger
clear all
dims = [1024,1280];
f1 = 1;% first length has focal length 1m
f2 = 20e-2; % second lens had focal length 20cm
ftl = 12.5e-2; % tube length is 12.5cm
M0 = 63; % objective magnification
NA = 1.4; % objective NA
wavelength = 561e-9;
slm_pitch = 13.62e-6;
f0 = 0.165 / M0;
lambda = [488,561,637]*10^-9;
names = {'488','561','637'};

%% some TIRF images
% compute the period of the pattern so that the beam is is centered in the tirf zone
Rtirf =  f0 * tan(asin(1.33/1.52)) * f2 / ftl;
Robj =  f0 * tan(asin(NA/1.52)) * f2 / ftl;
Rtirfcenter = (Rtirf + Robj) / 2;
period = lambda./(slm_pitch*sin(atan(Rtirfcenter/f1)));
fprintf('TIRF SIM mask radius is %fmm diameter %fmm periods are [%.2f,%.f,%.2f]\n', 1000*Rtirfcenter,  1000*(Robj-Rtirf), period)
%%
tag = 'tirf';
h = fopen(sprintf('data/%s-info.txt',tag),'w');
fprintf(h, 'TIRF SIM mask radius is %fmm\nDiameter %fmm\nperiods are [%.2f,%.f,%.2f]\n', 1000*Rtirfcenter,  1000*(Robj-Rtirf), period);
fprintf(h, 'f1:%f, f2:%f, ftl:%f M0:%f NA%f\n',f1,f2,ftl,M0,NA);
fclose(h);
dims = [1024, 1280];
for k = 1:numel(period)
  param = generate_sim_parameters(period(k),3,3);
  M = generate_sim_modulation(dims, param, 0, false);
  for i = 1:size(M,3)      
    filename =  sprintf('data/%s-%s-%02d.png',tag, names{k}, i);      
    fprintf('Saving %s\n', filename);
    imwrite(logical(M(:,:,i)),  filename);    
  end
end

%% SIM
% generate 3 set of images for each wavelength which is 256nm in the sample
%
period = lambda/lambda(2) * 12.5;
R = f1 * tan(asin(lambda/(period*slm_pitch)));
fprintf('SIM mask radius is %fmm \n', R*1000)
fprintf('SIM mask radius is %fmm, diameter %fmm periods are [%.2f,%.f,%.2f]\n', 1000*R,  2, period)
%%
tag = 'sim';
h = fopen(sprintf('data/%s-info.txt',tag),'w');
fprintf(h, 'TIRF SIM mask radius is %fmm, diameter %fmm periods are [%.2f,%.f,%.2f]\n', 1000*R,  2, period);
fprintf(h, 'f1:%f, f2:%f, ftl:%f M0:%f NA%f\n',f1,f2,ftl,M0,NA);
fclose(h);
dims = [1024, 1280];
for k = 1:numel(period)
  param = generate_sim_parameters(period(k),3,3);
  M = generate_sim_modulation(dims, param, 0, false);
  for i = 1:size(M,3)            
    filename =  sprintf('data/%s-%s-%02d.png',tag, names{k}, i);      
    fprintf('Saving %s\n', filename);
    imwrite(logical(M(:,:,i)),  filename);
  end
end

%% A sequence
period = 2:15;
names = strsplit(sprintf('%02d,',period),',');
tag = 'seq';
dims = [1024, 1280];
for k = 1:numel(period)
  param = generate_sim_parameters(period(k),1,1);
  M = generate_sim_modulation(dims, param, 0, false);  
  filename =  sprintf('data/%s-%s.png',tag, names{k});      
  %fprintf('Saving %s\n', filename);
  imwrite(logical(M(:,:,1)),  filename);  
end

