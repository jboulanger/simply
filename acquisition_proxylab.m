warning('off','images:imshow:magnificationMustBeFitForDockedFigure')
addpath(genpath('../proxylab'));
% select a file
if exist('d','var') == 0
    d = '../../data/';
end
[f, d] = uigetfile([d '*.tif']);
filename = [d, f];

%%
% load the data in a 3D array
metadata = imfinfo(filename);
fprintf('Loading %d frames from ''%s''\n', numel(metadata), filename);
data = zeros(metadata(1).Height,metadata(1).Width,numel(metadata));
for l = 1:numel(metadata)
  data(:,:,l) = double(imread(filename, l));
end
fprintf('Image size %dx%dx%d\n', size(data));

% padding in case images are not square
if size(data,1) < size(data,2)
    disp('Padding the first dimension')
    P = (size(data,2)-size(data,1));
    data = padarray(data,[P,0,0],min(data(:)),'post');
    fprintf('Image size after padding %dx%dx%d\n', size(data));
else   
    disp('Padding the second dimension')
    P = (size(data,1)-size(data,2));
    data = padarray(data,[0,P,0],min(data(:)),'post');
    fprintf('Image size after padding %dx%dx%d\n', size(data));
end


% load or define microscopy parameters
pfile = [d strrep(f,'.tif','.mat')];
if exist(pfile,'file')
    fprintf('Loading existing parameter file %s\n', pfile);
    load(pfile);
else 
    if ~exist('answer', 'var')
        answer = {'82.9','580','1.49'};
    end
    answer = inputdlg({'Pixel_size','Wavelength','NA'},...
        'Microscope parameter', 1, answer);
    pixel_size = str2double(answer{1});
    wavelength = str2double(answer{2});
    numerical_aperture = str2double(answer{3});
    fprintf('Saving parameters to file ''%s''.\n', pfile);
    save(pfile,'pixel_size','wavelength','numerical_aperture');
end

cutoff = wavelength / (2 * pixel_size * numerical_aperture);
fprintf('px:%.2fnm, em wavelength:%.0fnm, NA:%.2f, cuttoff:%.2fpx\n', ...
    pixel_size, wavelength, numerical_aperture, cutoff);

%%
cutoff = 2.5;
otf0 = generate_otf(size(data,1), cutoff);
%% normalize
data = normalize_sim_intensity(data,otf0);
%% estimate modulations
tic
otf0 = generate_otf(size(data,1), cutoff);
phat = estimate_sim_parameters(data,otf0);
display_sim_parameter(phat,pixel_size,wavelength,numerical_aperture)
toc
%%
data = data - 100;
for k=1:size(data,3) 
    data(:,:,k) = tape(data(:,:,k),0.1);
end
clf, imshow(mean(data,3),[])
%%
zoom = 2;
[im0,sw,C] = reconstruct_sim_base(data,phat,otf0,zoom,10,1);
im0 = max(0,im0);
imshow(im0,[])

%% Reconstruction
clear opts;
dims = zoom * [size(data,1), size(data,2)];
otf = generate_otf(dims(1), zoom*cutoff);
prec = phat;
for i=1:numel(phat)
    prec(i).period = zoom * phat(i).period;
    prec(i).amplitude = prec(i).amplitude;
end
M = generate_sim_modulation(dims, prec);
M = M - mean(M,3) + 0.5;
%%
figure(1), clf;
opts.init = im0;
opts.max_iter = 100;
opts.record = 0;
opts.sigma = 0.01;
opts.tau = 1./(opts.sigma);
opts.rho = .9;
opts.observer = @observer_fft;
opts.bounds = [0,Inf];
[im, etime, niter, rnorm] = reconstruct_sim(otf, M, 1/zoom, data, 'l2','tv',1,opts);
fprintf('Elspased time %.2fs\n', etime);
figure(1);
subplot(221), imshow(mean(data,3),[]);
subplot(222), fftshow(mean(data,3),otf);
subplot(223), imshow(im,[0 max(im(:))])
subplot(224), fftshow(im,otf);
%% create operators
A = create_structured_illumination_op(otf, M, 1/zoom, 'spectral', true);
La = 3;
%La = compute_operator_norm(A,randn(size(data,1)*zoom,size(data,2)*zoom),20);
fprintf('norm of the SIM operator = %f\n',La);
%M = M ./ La;
%A = create_structured_illumination_op(otf, M, 1/zoom, 'spectral', true);
clear cost;
cost(1) = create_cost_term(create_l2norm_fun(1, data), A);
cost(2) = create_regularization_term(0.1, 'tv', 2);
%S = sum(abs(fft2(M)),3);
%S = S / max(S(:));
%Op = create_convolution_op(S,'spectral',true);
%cost(3) = create_cost_term(create_l2norm_fun(10),Op);
%Lc = compute_multi_operator_norm(cost,randn(size(data,1)*zoom,size(data,2)*zoom),10);
Lc = 9;
fprintf('|sum T*T| = %f\n',Lc);

figure(1), clf;
opts.max_iter = 500;
opts.record = 0;
opts.sigma = 0.1;
opts.tau = 1./(Lc*opts.sigma);
opts.rho = .9;
opts.observer = @observer_fft;
output = pdhg(cost, opts);
im = output.estimate;
%%
figure(2);
subplot(131), imshow(mean(data,3),[]), title('wf');
subplot(132), imshow(im0,[]), title('wiener');
subplot(133), imshow(im,[]), title('l2-tv');

%%
ofilename = strrep(filename,'.tif','_sim_proxylab.tif');
fprintf('Saving 16bit rescaled file ''%s''\n', ofilename);
ims = uint16( 2^16 * (im - min(im(:))) / (max(im(:))-min(im(:))));
imwrite(ims,ofilename,'Compression','lzw');

ofilename = strrep(filename,'.tif','_wf.tif');
fprintf('Saving 16bit rescaled file ''%s''\n', ofilename);
wf = mean(data,3);
ims = uint16( 2^16 * (wf - min(wf(:))) / (max(wf(:))-min(wf(:))));
imwrite(ims,ofilename,'Compression','lzw');
