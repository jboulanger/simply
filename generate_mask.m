function generate_mask(f1,f2,ftl,f0,NA,wavelength,slm_pitch,period,N,D)
  % generate_mask(f1,f2,ftl,f0,NA,wavelength,slm_pitch,period,N,D)
  %
  % f1 : focal length of lens 1
  % f2 : focal length of lens 2
  % ftl : focal lenght of tube lens
  % f0 : objective focal length
  % NA : numerical aperture
  % wavelength : array of wavelength
  % slm_pitch : pixel size of the SLM
  % period : pattern period
  % N : number of orientations
  % D : diameter of the hole
  %
  %
  % Jerome Boulanger 2020
  c = linspace(0,2*pi);
  cols = [0.2,0.8,0.5;0.8,0.8,0.1;0.8,0.2,0.2];
  cols = 0.5*(cols+ones(size(cols)));
  lambda = [488,561,637] * 1e-9;

  M = 1/(f2/f1*f0/ftl);
  % radius of the 1st diffraction orders
  R = f1 * tan(asin(wavelength/(period*slm_pitch)));
  % angle
  theta_m = asin(wavelength/(period*slm_pitch)) * M;

  % radius of the NA of the objective
  r0 = f0 * tan(asin(NA/1.52)); % at the BFP
  R0 = r0 * f2 / ftl; % at the mask

  % radius of TIRF angle
  rc = f0 * tan(asin(1.33/1.52));
  Rc = rc * f2 / ftl; % at the mask

  % modulation in the sample
  ps = 0.5 * period * slm_pitch / M;
  fprintf(1,'Modulation period in the sample %.1fnm\n',ps*1e9);

  % 1" mask
  plot(0.0254/2*cos(c), 0.0254/2*sin(c), 'k', 'linewidth', 2)
  hold on

  % NA in the back focal plane
  plot(R0*cos(c), R0*sin(c), 'color',[1 0.2 1], 'linewidth', 1);
  plot(Rc*cos(c), Rc*sin(c), 'color',[0.2 1 0.2], 'linewidth', 1);
  for i = 1:N
    for j = 1:numel(lambda)
      rw = f1 * tan(asin(lambda(j)/(period*slm_pitch)));
      xw = rw * cos(2*pi*i/N);
      yw = rw * sin(2*pi*i/N);
      plot(xw,yw,'.','color',cols(j,:),'markersize',10)
    end
    x0 = R * cos(2*pi*i/N);
    y0 = R * sin(2*pi*i/N);
    plot(x0 + D/2*cos(c), y0 + D/2*sin(c),'k','linewidth', 1);
  end
  hold off
  axis equal
  axis off

  %title(sprintf('Outer diameter: 25.4mm \nNA=%.2f\n Center wavelength:%dnm period:%dpx\nR=%.2fmm r=%.2fmm',NA, round(wavelength*1e9), period, R*1e3, r*1e3))
  title(sprintf('Modulation period:%dpx\nNumber of holes:%d\nCircle radius:%.2fmm\nHole diameter=%.2fmm', period, N, R*1e3, D*1e3))
