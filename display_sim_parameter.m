function display_sim_parameter(p,pixel_size,wavelength,numerical_aperture)
%
% display_sim_parameter(p,pixel_size,wavelenght,numerical_aperture)
% display_sim_parameter(p)
%
% Display estimated SIM parameters
%
% Jerome Boulanger 2018

fprintf(1,'period\tangle\tampl.(%%)\tshift(%%)\n');
for l = 1:numel(p)
    fprintf(1,'%.3f\t%.2f\t%.3f\t%.2f\n', p(l).period, p(l).orientation,...
        p(l).amplitude*100,p(l).shift*100);
end

if nargin > 1
    r0 = wavelength / (2*numerical_aperture);
    p0 = mean([p(:).period]) * pixel_size;
    fprintf(1,'period %dnm -> %d%% cutoff\n',  round(p0), round(100*r0 / p0));
end

 fprintf(1,'amplitude avg : %.2f%%\n',  mean([p(:).amplitude])*100);

% analysis of phase distribution
tol = 5;
theta = unique(round([p(:).orientation]/tol)*tol);
for k = 1:numel(theta)
    idx = find(abs([p(:).orientation]-theta(k)) < tol);
    thetak = mean([p(idx).orientation]);
    s = [p(idx).shift];
    ep(k) = sqrt(mean((mod(s-circshift(s,1),1) - 1/3).^2));    
end
fprintf(1,'phase spread RMS error %.2f:%.2f, %.2f:%.2f, %.2f:%.2f\n', [theta(:),ep(:)]');

