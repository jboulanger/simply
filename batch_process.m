function [files,param_acq,param_rec]=batch_process(files, param_acq, param_rec)
% batch_process(files, param_acq, param_rec)
%
% If any of the 3 parameters is empty a graphical prompt is used to ask for
% the corresponding values.
%
% Input:
%  files : 
% 
if ~iscell(files)
    [files, d] = uigetfile([files '*.tif'],'MultiSelect', 'on');    
end

fprintf('Number of files: %d\n', numel(files));

% define acquisition parameters
if isempty(param_acq)
    answer = {'82.9','580','1.49'};
    answer = inputdlg({'Pixel_size','Wavelength','NA'},...
        'Acquisition parameter', 1, answer);
    pixel_size = str2double(answer{1});
    wavelength = str2double(answer{2});
    numerical_aperture = str2double(answer{3});   
    param_acq = [pixel_size,wavelength,numerical_aperture];
else
    pixel_size = param_acq(1);
    wavelength = param_acq(2);
    numerical_aperture = param_acq(3);
end

cutoff = wavelength / (2 * pixel_size * numerical_aperture);

fprintf('px:%.2fnm, em wavelength:%.0fnm, NA:%.2f, cuttoff:%.2fpx\n', ...
    pixel_size, wavelength, numerical_aperture, cutoff);


if isempty(param_rec)
    answer = {'2','1','1'};
    answer = inputdlg({'Zoom','Regularization','Mask'},...
        'Reconstruction parameter', 1, answer);
    zoom = str2double(answer{1});
    wiener_parameter = str2double(answer{2});
    mask_amplitude = str2double(answer{3});  
    param_rec = [zoom,wiener_parameter,mask_amplitude];
else
    zoom = param_rec(1);
    wiener_parameter = param_rec(2);
    mask_amplitude = param_rec(3);
end

for n = 1:numel(files)
    filename = [d filesep files{n}];
    fprintf('Loading ''%s''\n', filename);
    metadata = imfinfo(filename);
    data = zeros(metadata(1).Height,metadata(1).Width,numel(metadata));
    for l = 1:numel(metadata)
        data(:,:,l) = double(imread(filename, l));
    end
    
    % padding in case images are not square
    if size(data,1) < size(data,2)    
        P = (size(data,2)-size(data,1));
        data = padarray(data,[P,0,0],min(data(:)),'post');
    end

    if size(data,1) > size(data,2)    
        P = (size(data,1)-size(data,2));
        data = padarray(data,[P,0],min(data(:)),'post');
    end
    
    % Create the OTF    
    otf = generate_otf(max(size(data)), cutoff);
    
    % Modulation estimation
    phat = estimate_sim_parameters(data,otf);
    display_sim_parameter(phat,pixel_size,wavelength,numerical_aperture)
    
    % 
    zoom = 1; % magnification to accomodate extra resolution (2)
    wiener_parameter = 10; % noise level [1e3:1e-4]
    mask_amplitude = 0.7; % sectionning [0:1]
    im = reconstruct_sim_base(data,phat,otf,zoom, ...
        wiener_parameter,mask_amplitude);
    
    ofilename = strrep(filename,'.tif','_sim.tif');    
    fprintf('Saving 16bit rescaled file ''%s''\n', ofilename);
    ims = uint16( 2^16 * (im - min(im(:))) / (max(im(:))-min(im(:))));
    imwrite(ims,ofilename,'Compression','lzw');
end
    